package ru.t1.volkova.tm.exception.entity;

public final class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! Session Not found...");
    }

}
